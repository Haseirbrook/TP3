// Port to listen requests from
var port = 1234;

// Modules to be used
var shajs = require('sha.js');
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var randtoken = require('rand-token');
var cookieParser = require('cookie-parser')

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });
// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files

//app.use(express.static('public'));
app.use(cookieParser());
app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});
app.get('/', function(req, res) {
    db.all("SELECT token FROM sessions WHERE token=?",[req.cookies['Token']],function (err, data)
    {
        if (data.length>0) {

            //res.json({status: (true), token: ("Vous etes deja connecte")})
            res.sendfile('public/Deconnecter.html')  //Affichage de la page en etant deja Log sur //localhost:1234/
        }
        else
        {
            res.sendfile('public/index.html');
        }
        //res.json(data)
    });
    //res.sendfile('public/index.html');
});
app.post("/deco", function (req, res, next) {
   //var shatoken = shajs('sha256').update(Date.now() + req.body.log + req.body.password).digest('hex');
   // db.all('UPDATE sessions SET token=? WHERE token=?', [shatoken,req.cookies['Token']]); //generation d'un token bidon pour fermer la session
    //res.cookie('Token', shatoken); //Sets name = express
    res.clearCookie('Token'); //destruction du token
    res.sendfile('public/index.html');
});
app.post("/login", function (req, res, next) {
 /*   db.all('SELECT rowid, ident, password FROM users;', function (err, data) {
        //res.json(data);
    });*/
    var token = randtoken.generate(16); //Genere un token sur 16 caracteres
    //res.send(token);
    
    db.all("SELECT * FROM users WHERE ident=? AND password =?;", [req.body.log, req.body.password], function (err, data) {
        var shatoken = shajs('sha256').update(Date.now() + req.body.log + req.body.password).digest('hex'); //generation d'un token a l'aide de la date et du login

        if(data.length > 0 )
        {
            db.all('UPDATE sessions SET token=? WHERE ident=?', [shatoken,req.body.log]);
            //express.res.cookie
            res.cookie('Token', shatoken); //Sets name = express
            //res.json({ status: (true),token: (shatoken) })
            res.sendfile('public/Deconnecter.html')
            //console.log('CookiesVERYVrAI: ', req.cookies)
        }
        else
        {
                    //res.sendfile('public/Deconnecter.html')
                    res.json({status: (false), token: ("Erreur MDP")})
        }
    });
    //res.send(req.body.Login);
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
